import React, { Component } from 'react';
import Header from './Components/Header';
import TodoList from './Components/TodoList';

import './App.css';

class App extends Component {
  constructor(){
    super();

    this.state = {
      todoList: [
        {id: Math.random(), text: 'kupić mleko.', done: false},
        {id: Math.random(), text: 'zadzwonić do Jasia', done: false},
        {id: Math.random(), text: 'umyć naczynia', done: false},
        {id: Math.random(), text: 'pouczyć się react-a', done: false},
      ]
    };
  }

  addNewTodo = (text) => {
    const newTodo = {
      id: Math.random(),
      text: text,
      done: false
    }

    this.setState({
      ...this.state,
      todoList: [...this.state.todoList, newTodo]
    })
  }

  removeElement = (id) => {
    const newTodoList = [...this.state.todoList].filter(v => v.id !== id);

    this.setState({
      ...this.state,
      todoList: [...newTodoList]
    })
  }

  editValue = (id, value) => {
    const elementIndex = [...this.state.todoList].findIndex(v => v.id === id);
    
    const newTodoList = [...this.state.todoList];
    newTodoList[elementIndex].text = value;

    this.setState({
      ...this.state,
      todoList: [...newTodoList]
    })
  }

  switchStatus = (id) => {
    const elementIndex = [...this.state.todoList].findIndex(v => v.id === id);

    const newTodoList = [...this.state.todoList];
    newTodoList[elementIndex].done = !newTodoList[elementIndex].done;

    this.setState({
      ...this.state,
      todoList: [...newTodoList]
    })
  }

  render() {
    return (
      <div className="app-container">
        <div className="app">
          <Header addTodo={this.addNewTodo} />
          <TodoList 
            {...this.state} 
            removeElement={this.removeElement} 
            switchStatus={this.switchStatus} 
            editValue={this.editValue} 
          />
        </div>
      </div>
    );
  }
}

export default App;
