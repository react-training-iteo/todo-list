# React Szkolenie - Todo List - simple

<img src="./src/assets/todolist.png" />

## Dostępne komendy:

W folderze projektu dostępne są następujące komendy:

**Instalacja zależności (pierwsze uruchomienie)**
### `npm install`

**Start lokalnego serwera w trybie deweloperskim**
### `npm start`

W przeglądarce: [http://localhost:3000](http://localhost:3000) adres lokalnego serwera.

**Uruchamianie testów**
### `npm test`

**Tworzenie wersji produkcyjnej**
### `npm run build`
gotowy produkt znajduje się w folderze: build


**Dodanie plików konfiguracyjnych do folderu projektu**
#### `npm run eject`
**WAŻNE: użycie operacji EJECT nie pozwala na przywrócenie poprzedniego stanu!**


## Podstawowe elementy 

### Implementacja komponentu z użyciem state
```JavaScript 
  import React from 'react'

  class MyComponent extends React.Component {
    constructor(){
      super();
      this.state = {
        name: 'Jan'
      }
    }

    handleClick = () => {
      this.setState({
        name: 'Kasia'
      })
    }

    render(){
      const {name} = this.state;	
      return (
        <div onClick={this.handleClick}>Witaj {name}!</div>
      )
    }
  }
```


### Pzekazywanie parametrów do komponentu (props) 

```JavaScript 
  class PersonalData extends React.Component {
    render(){
      const {name, surname, age} = this.props;	
      return (
      <ul>
        <li>Imię: {name}</li>
        <li>Nazwisko: {surname}</li>
        <li>Wiek: {age}</li>
      </ul>
      )
    }
  }
  class MyComponent extends React.Component {
    render(){
      return (
        <PersonalData  name="Jan" surname="Kowalski" age={32} />
      )
    }
  }
```  